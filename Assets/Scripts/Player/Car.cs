﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Car : MonoBehaviour
{
    public float maxSpeed = 60f;
    float speed = 0f;
    public GameObject gameOver;

    void Update()
    {
        transform.Translate(0, 0, speed * Time.deltaTime);

        if (speed < maxSpeed)
        {
            speed += 0.02f;
        }

        if (maxSpeed > 80)
        {
            maxSpeed = 80;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Cat") || other.CompareTag("Penguin") || other.CompareTag("Rabbit"))
        {
            StartCoroutine(GameOverDelay());
        }
    }

    IEnumerator GameOverDelay()
    {
        gameOver.SetActive(true);
        yield return new WaitForSeconds(2f);
        Time.timeScale = 0f;
    }
}
