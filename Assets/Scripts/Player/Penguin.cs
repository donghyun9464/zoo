﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Penguin :MonoBehaviour
{
    public bool isJump = false;
    bool grounded = true;
    Animator animator;
    public float jumpPower = 500f;
    Rigidbody rb;
    public GameObject player;
    public SfxManager SfxManager;

    private void Start()
    {
        animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
    }
    private void Update()
    {
        transform.position = new Vector3(transform.position.x, transform.position.y, player.transform.position.z);
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            Jump();
        }
    }

    public void Jump()
    {
        if (grounded)
        {
            rb.AddForce(0, jumpPower, 0);
            animator.SetBool("isJump", true);
            grounded = false;
        }
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.collider.CompareTag("Ground"))
        {
            animator.SetBool("isJump", false);
            grounded = true;
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Obstacle")) // 장애물과의 충돌
        {
            Players.instance.SpeedDown();
        }

        if (other.CompareTag("SpeedBead"))
        {
            Players.maxSpeed += 5;
        }

        if (other.CompareTag("Car"))
        {
            rb.AddForce(0, 30000f, 0);
        }
    }
}
