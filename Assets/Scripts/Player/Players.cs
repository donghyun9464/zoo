﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Players : MonoBehaviour
{
 
    public static float maxSpeed = 50f;
    public float speed = 0f;
    public static Players instance;

    void Awake()
    {
        if (Players.instance == null)
            Players.instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(0, 0, speed * Time.deltaTime);
        if (speed < maxSpeed)
        {
            speed += 0.05f;
        }

        if (maxSpeed > 80)
        {
            maxSpeed = 80;
        }
    }
    public void SpeedDown()
    {
        speed = 0;
        
    }
    
}
