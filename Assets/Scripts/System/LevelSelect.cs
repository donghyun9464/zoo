﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelSelect : MonoBehaviour {
    public GameObject leaderBoardWindow;
    public GameObject mapSelectWindow;
    public GameObject settingsWindow;
    public Color hovercolor;
    private Renderer rend;
    private Color startColor;
    public GameObject LBcancelButton;
    public GameObject MScancelButton;
    public GameObject STcancelButton;
    public static int mapNumber;
    
	void Start () {
        rend = GetComponent<Renderer>();
        startColor = rend.material.color;
        leaderBoardWindow.SetActive(false);
        mapSelectWindow.SetActive(false);
        settingsWindow.SetActive(false);
	}
    public void cancelLB()
    {
        leaderBoardWindow.SetActive(false);
    }
    public void cancelMS()
    {
        mapSelectWindow.SetActive(false);
    }
    public void cancelST()
    {
        settingsWindow.SetActive(false);
    }

    public void OpenLeaderBoard()
    {
        leaderBoardWindow.SetActive(true);
    }

    public void OpenSettings()
    {
        settingsWindow.SetActive(true);
    }

    void OnMouseDown()
    {
        mapSelectWindow.SetActive(true);
    }
    void OnMouseEnter()
    {
        rend.material.color = hovercolor;
    }
    void OnMouseExit()
    {
        rend.material.color = startColor;
    }

    public void PlayStageOne()
    {
        mapNumber = 1;
        SceneManager.LoadScene("ZooDesert");
    }
    public void PlayStageTwo()
    {
        mapNumber = 2;
        SceneManager.LoadScene("ZooWoods");
    }
    public void PlayStageThree()
    {
        mapNumber = 3;
        SceneManager.LoadScene("ZooTundra");
    }
}
