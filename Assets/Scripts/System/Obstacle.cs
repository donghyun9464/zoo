﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour {
    public ParticleSystem wrongEffect;
    public GameObject parent;

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Car"));

        else
        {
            Instantiate(wrongEffect, transform.position, Quaternion.identity);
        }
        if (parent != null)
        {
            Destroy(parent);
        }
    }
}
