﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {
    private int Timer = 0;

    public GameObject Num_READY;
    public GameObject Num_GO;
    public GameObject UIpanel;
    void Start()
    {
        //시작시 카운트 다운 초기화
        Timer = 0;

        Num_READY.SetActive(false);
        Num_GO.SetActive(false);
    }
    public void countReset()
    {
        UiSoundManager.instance.PlaySoundGo();
        UIpanel.SetActive(false);
        Timer = 0;
    }


    void Update()
    {
        //Setting();
        //게임 시작시 정지
        if (Timer == 0)
        {
            UiSoundManager.instance.PlaySoundReady();
            Time.timeScale = 0.0f;
        }
        //Timer 가 120보다 작거나 같을경우 Timer 계속증가

        if (Timer <= 60)
        {
            Timer++;
            
            if (Timer >15)
            {
                //Num_A.SetActive(false);
                Num_READY.SetActive(true);
                
            }

            //Timer 가 120보다 크거나 같을경우 1번끄고 GO 켜기 LoadingEnd () 코루틴호출 
            if (Timer >=45 )
            {
                UiSoundManager.instance.PlaySoundGo();
                Num_READY.SetActive(false);
                Num_GO.SetActive(true);
                StartCoroutine(this.LoadingEnd());
                Time.timeScale = 1.0f; //게임시작
               
            }
        }

    }
    IEnumerator LoadingEnd()
    {
        yield return new WaitForSeconds(1.0f);
        Num_GO.SetActive(false);
    }

}
