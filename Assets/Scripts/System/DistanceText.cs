﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DistanceText : MonoBehaviour
{
    public static int distance;
    public GameObject player;
    public GameObject car;
    Text text;

    void Awake()
    {
        text = GetComponent<Text>();
        distance = 0;
    }

    // Update is called once per frame
    void Update()
    {
        distance = (int)(player.transform.position.z - car.transform.position.z);
        text.text = "DISTANCE : " + distance + "M";
    }
}
