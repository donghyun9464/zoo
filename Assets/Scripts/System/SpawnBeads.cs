﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnBeads : MonoBehaviour {
    public List<GameObject> beads;      // 7가지의 구슬 순서
    public float speed = 20;            // 구슬 생성 지점이 앞으로 이동하는 속도
    public float spawnDelay = 1.0f;     // 구슬 생성 간격

    // Use this for initialization
    void Start() {
        StartCoroutine(SpawnObjects());
    }

    // Update is called once per frame
    void Update() {
        transform.Translate(0, 0, speed * Time.deltaTime);
    }

    // 구슬 랜덤 생성
    IEnumerator SpawnObjects()
    {
        int t = 0;
        while (true)
        {
            int selection = Random.Range(0, 7);
            Instantiate(beads[selection], transform.position, Quaternion.identity);
            yield return new WaitForSeconds(spawnDelay);
        }
    }
}
