﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapManagerWood : MonoBehaviour
{
	bool isCreated = false;
	public GameObject map;
	private void OnTriggerEnter(Collider other)
	{
		if (!isCreated)
		{
			Instantiate(map, new Vector3(transform.parent.transform.position.x, transform.parent.transform.position.y, transform.parent.transform.position.z + 2700), Quaternion.identity);
			Destroy(transform.parent.gameObject, 20f);
			isCreated = true;
		}
	}
}
