﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BgmManager : MonoBehaviour
{
    public AudioClip DesertBgm;
    AudioSource myBGM;

    public static BgmManager instance;

    void Awake()
    {
        if (BgmManager.instance == null)
            BgmManager.instance = this;
    }

    void Start()
    {
        myBGM = GetComponent<AudioSource>();
    }

    public void DesertBGM()
    {
        myBGM.PlayOneShot(DesertBgm);
    }
    // Update is called once per frame
    void Update()
    {

    }
}
