﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UiSoundManager : MonoBehaviour
{
    public AudioClip soundUI;
    public AudioClip soundReady;
    public AudioClip soundGo;
    public AudioClip soundLevelUp;
    public AudioClip soundGameOver;

    AudioSource myUIsound;

    public static UiSoundManager instance;

    void Awake()
    {
        if (UiSoundManager.instance == null)
            UiSoundManager.instance = this;
    }

    // Use this for initialization
    void Start()
    {
        myUIsound = GetComponent<AudioSource>();
    }

    public void PlaySoundUI()
    {
        myUIsound.Stop();
        myUIsound.PlayOneShot(soundUI);
    }
    public void PlaySoundReady()
    {
        myUIsound.Stop();
        myUIsound.PlayOneShot(soundReady);
    }

    public void PlaySoundGo()
    {
        myUIsound.Stop();
        myUIsound.PlayOneShot(soundGo);
    }


    public void PlaySoundLevelUp()
    {
        myUIsound.Stop();
        myUIsound.PlayOneShot(soundLevelUp);
    }

    public void PlaySoundGameOver()
    {
        myUIsound.Stop();
        myUIsound.PlayOneShot(soundGameOver);
    }
}
