﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour {
    public int scoreValueAdd = 100; //맞으면 +하는 점수
    public GameObject parent;

    public void ScoreUp()
    {
        ScoreText.score += scoreValueAdd;
    }

    private void OnTriggerEnter(Collider other)
    {
        ScoreUp();
        if(parent != null)
        {
            Destroy(parent, 5f);
        }
        Destroy(this.gameObject);
    }
}
