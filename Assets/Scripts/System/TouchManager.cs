﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchManager : MonoBehaviour
{
    public Cat cat;
    public Rabbit rabbit;
    public Penguin penguin;
    public Camera mainCam;
    // Update is called once per frame
    void Update()
    {
        for (var i = 0; i < Input.touchCount; ++i)
        {
            Ray ray = mainCam.ScreenPointToRay(Input.GetTouch(i).position);
            RaycastHit hit;
            if(Physics.Raycast(ray, out hit))
            {
                if(hit.collider.CompareTag("Cat"))
                {
                    cat.Jump();
                }
                if (hit.collider.CompareTag("Rabbit"))
                {
                    rabbit.Jump();
                }
                if (hit.collider.CompareTag("Penguin"))
                {
                    penguin.Jump();
                }
            }
        }
    }
}
