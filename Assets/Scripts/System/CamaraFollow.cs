﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamaraFollow : MonoBehaviour {
    public Transform player;
    float cameraX = 0;
    public float rotationSpeed = 0.5f;
    void Update () {
        if (cameraX <= 30)
        {
            Vector3 pos = transform.position;
            transform.eulerAngles = new Vector3(cameraX, 180, 0);
            cameraX += rotationSpeed;
        }
        // 캐릭터의 z축 위치를 따라 이동
        else
        {
            transform.eulerAngles = new Vector3(15, 0, 0);
            Vector3 pos = transform.position;
            pos.z = player.position.z - 25f;
            transform.position = pos;
        }

    }
}
// 0 15 -1210
// 15 0 0