﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedBead : MonoBehaviour
{
    public ParticleSystem beadEffect1;
    public ParticleSystem beadEffect2;
    public GameObject parent;
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Car"));
        else
        {
            Instantiate(beadEffect1, transform.position, Quaternion.identity);
            Instantiate(beadEffect2, transform.position, Quaternion.identity);
        }
        if (parent != null)
        {
            Destroy(parent);
        }

    }
}
