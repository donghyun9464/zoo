﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour {
    // Use this for initialization
    
    public GameObject loginWindow;
    public GameObject signupWindow;
    public GameObject aboutusWindow;

    public InputField loginEmail;
    public InputField loginPassword;

    public InputField signupEmail;
    public InputField signupPassword;
    public InputField confirmPassword;

    //Firebase.Auth.FirebaseAuth auth;

    AuthManager authManager;

    void Start() 
    {
        authManager = GetComponent<AuthManager>();
        //auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
        loginWindow.SetActive(false);
        signupWindow.SetActive(false);
        aboutusWindow.SetActive(false);
    }

    public void PlayGame()
    {
        ShowLoginWindow();
    }

    public void ShowLoginWindow()
    {
        signupWindow.SetActive(false);
        loginWindow.SetActive(true);
    }

    public void ShowSignupWindow()
    {
        loginWindow.SetActive(false);
        signupWindow.SetActive(true);
    }
    
    public void CancelLogin()
    {
        authManager.LoginresultText.text = "";
        loginWindow.SetActive(false);
    }

    public void CancelSignup()
    {
        authManager.SignupResultText.text = "";
        signupWindow.SetActive(false);
    }

    public void CancelAboutus()
    {
        aboutusWindow.SetActive(false);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void AboutUs()
    {
        aboutusWindow.SetActive(true);
    }

}
