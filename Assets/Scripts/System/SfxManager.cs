﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SfxManager : MonoBehaviour
{
    public AudioClip soundCorrect;
    public AudioClip soundWrong;
    public AudioClip soundSwap;

    AudioSource mySfx;

    public static SfxManager instance;

    void Awake()
    {
        if (SfxManager.instance == null)
            SfxManager.instance = this;
    }

    // Use this for initialization
    void Start()
    {
        mySfx = GetComponent<AudioSource>();
    }

    public void PlaySoundCorrect()
    {
        mySfx.Stop();
        mySfx.PlayOneShot(soundCorrect);
    }

    public void PlaySoundWrong()
    {
        mySfx.Stop();
        mySfx.PlayOneShot(soundWrong);
    }

    public void PlayTrigger()
    {
        mySfx.Stop();
    }
}
