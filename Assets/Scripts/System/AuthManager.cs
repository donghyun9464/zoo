﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Firebase;
using Firebase.Unity.Editor;

public class AuthManager : MonoBehaviour {

    MenuManager menuManager;

    bool LoginSuccess = false;

    Firebase.Auth.FirebaseAuth auth;
    public static Firebase.Auth.FirebaseUser user;

    public Text LoginresultText;
    public Text SignupResultText;

    void Start() 
    {
        
        menuManager = GetComponent<MenuManager>();
        InitializeFirebase();
        LoginSuccess = false;
    }

    public void PlayGame()
    {
        SceneManager.LoadScene("Loading");
    }

    public void LoginButton()
    {
        LoginWithFirebase(menuManager.loginEmail.text, menuManager.loginPassword.text);
        LoginStart();
    }

    public void CreateButton()
    {
        CreateWithFirebase(menuManager.signupEmail.text, menuManager.signupPassword.text, menuManager.confirmPassword.text);
    }

    public void SignOutButton()
    {
        auth.SignOut();
        SceneManager.LoadScene("Starting");
    }

    public void ShowResultLogin()
    {
        LoginresultText.text = LoginresultText.text + "";
    }

    public void ShowResultSignup()
    {
        SignupResultText.text = SignupResultText.text + "";
    }

    public void LoginStart()
    {
        if(LoginSuccess)
        {
            LoginresultText.text = "로그인 성공 \n 게임이 시작됩니다";
            Invoke("PlayGame", 1.5f);
        }
    }
    private void LoginWithFirebase(string email, string password)
    {
        auth.SignInWithEmailAndPasswordAsync(email, password).ContinueWith(task => 
        {
            if (task.IsCanceled) 
            {
                Debug.LogError("SignInWithEmailAndPasswordAsync was canceled.");
                LoginresultText.text = "로그인이 취소되었습니다.";
                LoginSuccess = false;
                Debug.Log(LoginSuccess);
                return;
            }
            if (task.IsFaulted) 
            {
                Debug.LogError("SignInWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                LoginresultText.text = "로그인중 에러가 발생했습니다.";
                LoginSuccess = false;
                Debug.Log(LoginSuccess);
                return;
            }

            Firebase.Auth.FirebaseUser newUser = task.Result;
            LoginSuccess = true;
            Debug.LogFormat("User signed in successfully: {0} ({1})",
                    newUser.DisplayName, newUser.UserId);
        });
    }

    private void CreateWithFirebase(string email, string password, string confirm)
    {
        if(password!=confirm) 
        {
            Debug.Log("You must match password and confirm password.");
            SignupResultText.text = "비밀번호가 일치하지 않습니다.";
            return;
        }
        auth.CreateUserWithEmailAndPasswordAsync(email, password).ContinueWith(task => 
        {
            if (task.IsCanceled) 
            {
                Debug.LogError("CreateUserWithEmailAndPasswordAsync was canceled.");
                SignupResultText.text = "계정 생성이 취소 되었습니다.";
                return;
            }
            if (task.IsFaulted) 
            {
                Debug.LogError("CreateUserWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                SignupResultText.text = "계정 생성중 오류가 발생했습니다.";
                return;
            }

            Firebase.Auth.FirebaseUser newUser = task.Result;
            Debug.LogFormat("Firebase user created successfully: {0} ({1})",
                    newUser.DisplayName, newUser.UserId);      
        });
        SignupResultText.text = "계정 생성에 성공했습니다.";
    }

    void InitializeFirebase()
    {
        auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
        auth.StateChanged += AuthStateChanged;
        AuthStateChanged(this, null);
    }

    void AuthStateChanged(object sender, System.EventArgs eventArgs)
    {
        if (auth.CurrentUser != user)
        {
            bool signedIn = user != auth.CurrentUser && auth.CurrentUser != null;
            if (!signedIn && user != null)
            {
                Debug.Log("Signed out " + user.UserId);
            }
            user = auth.CurrentUser;
            if (signedIn)
            {
                Debug.Log("Signed in " + user.UserId);
                //displayName = user.DisplayName ?? "";
                //emailAddress = user.Email ?? "";
                //photoUrl = user.PhotoUrl ?? "";
            }
        }
    }

    void OnDestroy()
    {
        auth.StateChanged -= AuthStateChanged;
    }

}
