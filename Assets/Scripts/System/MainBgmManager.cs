﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainBgmManager : MonoBehaviour
{
    public AudioClip MainBGM;
    AudioSource myMainBgm;

    public static MainBgmManager instance;

    void Awake()
    {
        if (MainBgmManager.instance == null)
            MainBgmManager.instance = this;
    }

    void Start()
    {
        myMainBgm = GetComponent<AudioSource>();
        DontDestroyOnLoad(transform.gameObject);
    }

    public void MainBgm()
    {
        myMainBgm.PlayOneShot(MainBGM);
    }
    // Update is called once per frame
    void Update()
    {
        if(Application.loadedLevelName == "ZooDesert")
        {
            myMainBgm.Stop();
        }
        if (Application.loadedLevelName == "ZooWoods")
        {
            myMainBgm.Stop();
        }
        if (Application.loadedLevelName == "ZooTundra")
        {
            myMainBgm.Stop();
        }
    }
}
