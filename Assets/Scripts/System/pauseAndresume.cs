﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;
public class pauseAndresume : MonoBehaviour {
    public bool pause;
    public GameObject pauseButton;
    public GameObject resumeButton;
    public GameObject restartButton;
    public GameObject UIpanel;
    // Use this for initialization
    void Start () {
        pause = false;
	}
	
    public void pressResume()
    {
        UiSoundManager.instance.PlaySoundUI();
        Time.timeScale = 1;
        UIpanel.SetActive(false);
    }
    public void pressRestartDes()
    {
        SceneManager.LoadScene("ZooDesert");
    }
    public void pressRestartWod()
    {
        SceneManager.LoadScene("ZooWoods");
    }
    public void pressRestartTun()
    {
        SceneManager.LoadScene("ZooTundra");
    }
    public void mainMenu()
    {
        SceneManager.LoadScene(2);
        MainBgmManager.instance.MainBgm();
    }
    public void onPause()
    {
        Debug.Log("pause!");
        pause = !pause;
        if(!pause)
        {
            Time.timeScale = 1;
            UIpanel.SetActive(false);
        }
        else if(pause)
        {
            UiSoundManager.instance.PlaySoundUI();
            Time.timeScale = 0;
            UIpanel.SetActive(true);
        }
    }

    public void endGame(int mapnum, int scoreval)
    {
        
    }
}
