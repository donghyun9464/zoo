﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Firebase;
using Firebase.Unity.Editor;
using Firebase.Database;

public class DatabaseManager : MonoBehaviour
{   
    public string a;

    List<Text> userList = new List<Text>();
    List<Text> scoreList = new List<Text>();
    public Text userA;
    public Text userB;
    public Text userC;
    public Text scoreA;
    public Text scoreB;
    public Text scoreC;
    public string[] usernames = new string[3];
    public string[] scores = new string[3];

    public Text currentUser;
    public Text currentUserTopscore;

    public string currentUser_str;
    public string currentUserTopScore_str;

    public static DatabaseReference reference;
    int currentDistance = 0;

    void Start()
    {
        userList.Add(userA);
        userList.Add(userB);
        userList.Add(userC);
        scoreList.Add(scoreA);
        scoreList.Add(scoreB);
        scoreList.Add(scoreC);
        FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://fire-88208.firebaseio.com/");
        reference = FirebaseDatabase.DefaultInstance.RootReference;
        currentUser_str = "";
        currentUserTopScore_str = "";
        ReadDataOnce();
        UpdateData();
    }

    public void ShowSettings()
    {
        currentUser.text = currentUser_str;
        currentUserTopscore.text = currentUserTopScore_str;

    }

    public void ShowLeaderBoard()
    {
        int j = 0;
        for(int i = 2; i >= 0; i--)
        {
            //Debug.Log(username+scorevalue);
            userList[i].text = usernames[j];
            scoreList[i].text = scores[j];
            j++;
        }
        Debug.Log("finish");
        return;
    }

    public void ReadDataOnce()
    {
        FirebaseDatabase.DefaultInstance
            .GetReference("users")
            .GetValueAsync().ContinueWith(task => {
                if (task.IsFaulted) {
                // Handle the error...
                }
                else if (task.IsCompleted) {
                    DataSnapshot snapshot = task.Result;
                    Debug.Log(snapshot.Child(AuthManager.user.UserId).Child("username"));
                    currentUser_str = snapshot.Child(AuthManager.user.UserId).Child("username").Value.ToString();
                    Debug.Log(snapshot.Child(AuthManager.user.UserId).Child("Topscore"));
                    currentUserTopScore_str = snapshot.Child(AuthManager.user.UserId).Child("Topscore").Value.ToString();
                }
            });
    }

    public void UpdateData()
    {
        FirebaseDatabase.DefaultInstance
            .GetReference("users")
            .ValueChanged += HandleValueChanged;
    }

    public void CreateData()
    {
        string userId = AuthManager.user.UserId;
        Dictionary<string, object> childCreates = new Dictionary<string, object>();

        childCreates["/users/" + userId + "/" + "username"] = AuthManager.user.Email;
        childCreates["/users/" + userId + "/" + "Desert"] = 0;
        childCreates["/users/" + userId + "/" + "Woods"] = 0;
        childCreates["/users/" + userId + "/" + "Tundra"] = 0;
        childCreates["/users/" + userId + "/" + "Maximum_Distance"] = 0;
        childCreates["/users/" + userId + "/" + "Topscore"] = 0;
        reference.UpdateChildrenAsync(childCreates).ContinueWith(
            task =>
            {
                Debug.Log(string.Format("Create :: Completed:{0} Canceled:{1} Faulted:{2}", task.IsCompleted, task.IsCanceled, task.IsFaulted));
            }
        );
    }

    public void RecordData()
    {
        string userId = AuthManager.user.UserId;    
        Dictionary<string, object> childUpdates = new Dictionary<string, object>();

        if(LevelSelect.mapNumber == 1)
            childUpdates["/users/" + userId + "/" + "Desert"] = ScoreText.score;
        else if(LevelSelect.mapNumber == 2)
            childUpdates["/users/" + userId + "/" + "Woods"] = ScoreText.score;
        else if(LevelSelect.mapNumber == 3)
            childUpdates["/users/" + userId + "/" + "Tundra"] = ScoreText.score;

        if(DistanceText.distance>currentDistance)    
            childUpdates["/users/" + userId + "/" + "Maximum_Distance"] = ScoreText.score;

        reference.UpdateChildrenAsync(childUpdates).ContinueWith(
            task =>
            {
                Debug.Log(string.Format("Record :: Completed:{0} Canceled:{1} Faulted:{2}", task.IsCompleted, task.IsCanceled, task.IsFaulted));
            }
        );
    }

    public void LoadData()
    {
        UpdateData();

        Debug.Log("load start!");
        //UpdateData();
        FirebaseDatabase.DefaultInstance
            .GetReference("users").OrderByChild("Topscore").LimitToLast(3)
            .GetValueAsync().ContinueWith(task =>{
                if (task.IsFaulted)
                {
                    Debug.Log("Data Load Error!");
                }
                else if (task.IsCompleted)
                {
                    DataSnapshot snapshot = task.Result;
                    int k = 0;
                    foreach (var item in snapshot.Children)
                    {
                        usernames[k] = item.Child("username").Value.ToString();
                        scores[k] = item.Child("Topscore").Value.ToString();
                        k++;
                        if(k>2) break;
                    }
                }
        });
          
        Debug.Log("load end!");   
    }
    
    void HandleValueChanged(object sender, ValueChangedEventArgs args) 
    {
        Debug.Log("changed");
        if (args.DatabaseError != null) {
            Debug.LogError(args.DatabaseError.Message);
            return;
        }
        if(!args.Snapshot.Child(AuthManager.user.UserId).Exists)
        {
            CreateData();
        }
        else
        {
            int max = 0;
            int topscore = int.Parse(args.Snapshot.Child(AuthManager.user.UserId).Child("Topscore").Value.ToString());
            int desscore = int.Parse(args.Snapshot.Child(AuthManager.user.UserId).Child("Desert").Value.ToString());
            int wodscore = int.Parse(args.Snapshot.Child(AuthManager.user.UserId).Child("Woods").Value.ToString());
            int tunscore = int.Parse(args.Snapshot.Child(AuthManager.user.UserId).Child("Tundra").Value.ToString());
            if(desscore>max)
            {
                max = desscore;
                Debug.Log(max);
            }
            if(wodscore>max)
            {
                max = wodscore;
                Debug.Log(max);
            }
            if(tunscore>max)
            {
                max = tunscore;
                Debug.Log(max);
            }
            string userId = AuthManager.user.UserId;    
            Dictionary<string, object> childUpdates = new Dictionary<string, object>();

            childUpdates["/users/" + userId + "/" + "Topscore"] = max;

            reference.UpdateChildrenAsync(childUpdates).ContinueWith(
                task =>
                {
                    Debug.Log(string.Format("Topscore changed :: Completed:{0} Canceled:{1} Faulted:{2}", task.IsCompleted, task.IsCanceled, task.IsFaulted));
                }
            );
        }
        
    }
}
